var gulp           = require('gulp'),
		gutil          = require('gulp-util' ),
		sass           = require('gulp-ruby-sass'),
		pug	           = require('gulp-pug'),
		browserSync    = require('browser-sync'),
		concat         = require('gulp-concat'),
		uglify         = require('gulp-uglify'),
		cleanCSS       = require('gulp-clean-css'),
		rename         = require('gulp-rename'),
		del            = require('del'),
		imagemin       = require('gulp-imagemin'),
		cache          = require('gulp-cache'),
		autoprefixer   = require('gulp-autoprefixer'),
		ftp            = require('vinyl-ftp'),
		notify         = require("gulp-notify");


// start browser + Live Reload 
gulp.task('browser-sync', function() {
	browserSync({
		server: {
			baseDir: 'dist'
		},
		notify: false,
	});
});

// compilation HTML
gulp.task('html', function buildHTML(){
	gulp.src('./app/*.pug')
		.pipe(pug({ 
		pretty: true
		}))
	.pipe(gulp.dest('./dist'))
	.pipe(browserSync.reload({stream: true}));
});

// compilation JS
gulp.task('common-js', function() {
	return gulp.src([
		'app/js/common.js',
		])
	.pipe(concat('common.min.js'))
	.pipe(uglify())
	.pipe(gulp.dest('app/js'));
});

gulp.task('js', ['common-js'], function() {
	return gulp.src([
		'app/libs/jquery/dist/jquery.min.js',
		'app/js/common.min.js', // Всегда в конце
		])
	.pipe(concat('scripts.min.js'))
	// .pipe(uglify()) // Минимизировать весь js (на выбор)
	.pipe(gulp.dest('dist/js'))
	.pipe(browserSync.reload({stream: true}));
});

// Sass to css
gulp.task('sass', function() {
	sass('app/scss/*.scss')
	.on('error', sass.logError)
	.pipe(autoprefixer({
			browsers: ['last 15 versions'],
			cascade: false
	}))
	.pipe(cleanCSS()) // Опционально, закомментировать при отладке
	.pipe(rename("styles.css"))
	.pipe(gulp.dest('dist/css'))
	.pipe(browserSync.reload({stream: true}));
});

////////////////////////////////////
/*
 * compilation web project
 */

gulp.task('build', ['removedist', 'imagemin', 'sass', 'js'], function() {

	//var buildFiles = gulp.src([
	//	'app/*.html',
	//	'app/.htaccess',
	//	]).pipe(gulp.dest('dist'));

	var buildCss = gulp.src([
		'app/css/main.min.css',
		]).pipe(gulp.dest('dist/css'));

	var buildJs = gulp.src([
		'app/js/scripts.min.js',
		]).pipe(gulp.dest('dist/js'));

	var buildFonts = gulp.src([
		'app/fonts/**/*',
		]).pipe(gulp.dest('dist/fonts'));

});

gulp.task('imagemin', function() {
	return gulp.src('app/img/**/*')
	.pipe(cache(imagemin()))
	.pipe(gulp.dest('dist/img')); 
});

gulp.task('removedist', function() { 
	return del.sync('dist'); 
});
gulp.task('clearcache', function () { 
	return cache.clearAll(); 
});


////////////////////////////////////

gulp.task('deploy', function() {

	var conn = ftp.create({
		host:      'hostname.com',
		user:      'username',
		password:  'userpassword',
		parallel:  10,
		log: gutil.log
	});

	var globs = [
	'dist/**',
	'dist/.htaccess',
	];
	return gulp.src(globs, {buffer: false})
	.pipe(conn.dest('/path/to/folder/on/server'));

});

////////////////////////////////////

gulp.task('watch', ['html', 'sass', 'js', 'browser-sync'], function() {
	gulp.watch('app/scss/**/*.scss', ['sass']);
	gulp.watch(['libs/**/*.js', 'app/js/common.js'], ['js']);
	gulp.watch('app/**/*.pug', ['html']);
});

gulp.task('default', ['watch']);
